## Musterlösung: Implementierung einer Pod-Authentifizierung

**Schritt 1: Erstellen eines Service Accounts**

Erstelle einen Service Account `auth-service-account.yaml`:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: auth-service-account
  namespace: default
```

Wende den Service Account an:

```bash
kubectl apply -f auth-service-account.yaml
```

**Schritt 2: Zuweisung des Service Accounts zu einem Pod**

Erstelle ein Deployment `auth-deployment.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: auth-app
  template:
    metadata:
      labels:
        app: auth-app
    spec:
      serviceAccountName: auth-service-account
      containers:
      - name: nginx
        image: nginx
```

Wende das Deployment an:

```bash
kubectl apply -f auth-deployment.yaml
```

**Schritt 3: Einrichtung von RBAC-Regeln**

Erstelle eine Rolle `pod-reader-role.yaml`:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: pod-reader
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "list"]
```

Wende die Rolle an:

```bash
kubectl apply -f pod-reader-role.yaml
```

Erstelle ein RoleBinding `pod-reader-binding.yaml`:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: pod-reader-binding
  namespace: default
subjects:
- kind: ServiceAccount
  name: auth-service-account
  namespace: default
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

Wende das RoleBinding an:

```bash
kubectl apply -f pod-reader-binding.yaml
```

**Schritt 4: Testen der Authentifizierung**

Identifiziere den Namen des Pods, der vom `auth-deployment` erstellt wurde:

```bash
kubectl get pods -l app=auth-app
```

Führe eine Remote-Shell im Pod aus und versuche, die Pods im Namespace aufzulisten:

```bash
kubectl exec -it [POD_NAME] -- /bin/sh
# Innerhalb der Shell
kubectl get pods
```

**Erwartetes Ergebnis:**
- Der Service Account `auth-service-account` ist korrekt erstellt und dem Deployment zugewiesen.
- Das Deployment `auth-deployment` läuft mit dem zugewiesenen Service Account.
- Die Rolle `pod-reader` und das RoleBinding sind korrekt konfiguriert und ermöglichen dem Pod den Zugriff auf Pod-Ressourcen im `default` Namespace.
- Der Test bestätigt, dass der Pod erfolgreich Authentifizierungs- und Autorisierungsprüfungen durchführen kann.

Diese Musterlösung zeigt, wie man Service Accounts, RBAC-Rollen und RoleBindings in Kubernetes einrichtet und nutzt, um die Authentifizierung und Autorisierung von Pods zu verwalten. Solche Kenntnisse sind essentiell für das Management von Kubernetes Clustern und für die CKAD-Prüfung.