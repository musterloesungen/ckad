Durch die in der Musterlösung beschriebene Vorgehensweise kann ein Pod innerhalb des Kubernetes-Clusters mit dem API-Server des eigenen Clusters kommunizieren. Hier sind die Schlüsselelemente dieser Kommunikation:

1. **Service Account**: Der Service Account bietet dem Pod eine Identität im Cluster, die für die Kommunikation mit dem Kubernetes API-Server verwendet wird.

2. **RBAC (Role-Based Access Control)**: Durch die Zuweisung einer Rolle und eines RoleBindings an den Service Account werden die Berechtigungen des Pods festgelegt. In diesem Fall erlaubt die Rolle `pod-reader` dem Pod, bestimmte Operationen (wie `get` und `list` auf Pods) durchzuführen.

3. **Automatische Einbindung von Credentials**: Kubernetes bindet automatisch die Credentials des Service Accounts in die Pods ein. Diese Credentials befinden sich standardmäßig im Verzeichnis `/var/run/secrets/kubernetes.io/serviceaccount/` innerhalb des Pods. Diese Credentials werden verwendet, um sich bei API-Anfragen zu authentifizieren.

4. **Kubectl im Pod**: Wenn `kubectl` innerhalb des Pods ausgeführt wird (wie im Schritt 4 der Musterlösung gezeigt), verwendet es diese Credentials, um mit dem Kubernetes API-Server zu kommunizieren. So kann der Pod Anfragen an den API-Server stellen, solange er die entsprechenden Berechtigungen hat.
