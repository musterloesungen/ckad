## Übungsaufgabe für CKAD Prüfung: Implementierung einer Pod-Authentifizierung

**Ziel:** Einrichten und Testen der Authentifizierung für einen Pod in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Erstellen eines Service Accounts**
    - Erstelle einen neuen Service Account mit dem Namen `auth-service-account` im Namespace `default`.

2. **Zuweisung des Service Accounts zu einem Pod**
    - Erstelle ein Deployment mit dem Namen `auth-deployment`.
    - Das Deployment soll einen Pod mit einem beliebigen Image (z.B. `nginx`) starten.
    - Weise dem Pod den `auth-service-account` zu.

3. **Einrichtung von RBAC-Regeln**
    - Erstelle eine Rolle `pod-reader`, die Berechtigungen zum Lesen von Pods im Namespace `default` hat.
    - Erstelle ein RoleBinding, das den `auth-service-account` mit der Rolle `pod-reader` verknüpft.

4. **Testen der Authentifizierung**
    - Überprüfe, ob der Pod mithilfe des Service Accounts auf Pod-Ressourcen im gleichen Namespace zugreifen kann.
    - Führe dies durch, indem du im Pod einen `kubectl get pods` Befehl ausführst.

**Hinweise:**
- Achte darauf, die RBAC (Role-Based Access Control) Regeln korrekt zu konfigurieren, um dem Pod die erforderlichen Berechtigungen zu gewähren.
- Verwende für den Test den `kubectl exec` Befehl, um in den Pod einzutreten und den Zugriff zu überprüfen.

**Erfolgskriterien:**
- Ein Service Account `auth-service-account` ist erfolgreich erstellt.
- Ein Deployment `auth-deployment` mit dem zugewiesenen Service Account ist erfolgreich gestartet.
- Die RBAC Konfiguration ermöglicht es dem Pod, Anfragen bezüglich Pod-Ressourcen im `default` Namespace auszuführen.
- Die Authentifizierung und Autorisierung des Pods wird erfolgreich durch den Test verifiziert.
