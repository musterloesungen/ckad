## CKAD Trainingsaufgabe: Erstellung eines Kubernetes CronJobs

1. Entwickle ein CronJob-Manifest für einen CronJob namens `ckad-cronjob` im Namespace `ckad-cron-ns`.
2. Der CronJob soll das Image `busybox` nutzen und alle 15 Minuten einen Job starten.
3. Jeder Job soll den Befehl `echo "Hello, CKAD CronJob"` ausführen.
4. Stelle sicher, dass der CronJob nur drei erfolgreiche Jobs behält und alte Jobs entsprechend bereinigt.
5. Der Namespace `ckad-cron-ns` soll erstellt werden, falls er noch nicht existiert.
6. Wende das CronJob-Manifest an und verifiziere, dass die Jobs wie geplant ausgeführt werden.
7. Stelle sicher, dass die Logs der ausgeführten Jobs den vorgesehenen Grußtext enthalten.
