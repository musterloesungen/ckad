## Musterlösung: YAML-Definition für Kubernetes CronJob

### CronJob Manifest

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: ckad-cronjob
  namespace: ckad-cron-ns
spec:
  schedule: "*/15 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            command: ["sh", "-c", "echo 'Hello, CKAD CronJob'"]
          restartPolicy: OnFailure
  successfulJobsHistoryLimit: 3
  failedJobsHistoryLimit: 1
```

### Namespace Manifest

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-cron-ns
```

### Anweisungen zur Anwendung und Überprüfung des CronJobs

1. Erstelle den Namespace `ckad-cron-ns` mit dem folgenden Befehl:

   ```bash
   kubectl apply -f namespace-manifest.yaml
   ```

2. Wende das CronJob-Manifest an, um den CronJob zu erstellen:

   ```bash
   kubectl apply -f cronjob-manifest.yaml
   ```

3. Überprüfe den Status des CronJobs, um sicherzustellen, dass er gemäß dem Zeitplan Jobs startet:

   ```bash
   kubectl get cronjobs -n ckad-cron-ns
   ```

4. Überprüfe die erstellten Jobs, um zu bestätigen, dass sie ausgeführt werden:

   ```bash
   kubectl get jobs -n ckad-cron-ns
   ```

5. Um die Logs eines spezifischen Jobs zu sehen, finde zunächst den Pod-Namen, der zum Job gehört:

   ```bash
   JOB_NAME=$(kubectl get jobs -n ckad-cron-ns -o=jsonpath='{.items[0].metadata.name}')
   POD_NAME=$(kubectl get pods -n ckad-cron-ns --selector=job-name=$JOB_NAME --output=jsonpath='{.items[*].metadata.name}')
   ```

6. Sieh dir die Logs des Pods an, um den Ausgabetext zu überprüfen:

   ```bash
   kubectl logs $POD_NAME -n ckad-cron-ns
   ```

   Wiederhole Schritt 5 und 6, falls mehrere Jobs vorhanden sind, um die Logs von verschiedenen Jobs zu überprüfen.
