## Übungsaufgabe für CKAD Prüfung: Einrichtung und Nutzung eines Kubernetes Secrets

**Ziel:** Erstellung eines Kubernetes Secrets zur Bereitstellung von Zugangsdaten in einem Pod.

**Aufgabenstellung:**

1. **Erstellung eines Kubernetes Secrets**
    - Erstelle ein Secret mit dem Namen `ckad-secret` im Namespace `ckad-secret-ns`.
    - Das Secret soll zwei Schlüssel-Wert-Paare beinhalten: `username` mit dem Wert `ckaduser` und `password` mit dem Wert `ckadpass`.

2. **Nutzung des Secrets in einem Pod**
    - Erstelle einen Pod mit dem Namen `ckad-secret-pod` im gleichen Namespace.
    - Der Pod soll einen Container enthalten, der das `busybox` Image nutzt.
    - Stelle die im Secret gespeicherten Zugangsdaten dem Container als Umgebungsvariablen zur Verfügung.

**Hinweise:**
- Achte darauf, das Secret korrekt zu definieren und sicherzustellen, dass die Schlüssel und Werte entsprechend den Anforderungen gesetzt sind.
- Überprüfe die korrekte Einbindung des Secrets in den Pod, um die Verfügbarkeit der Daten als Umgebungsvariablen sicherzustellen.

**Erfolgskriterien:**
- Ein Secret `ckad-secret` ist im Namespace `ckad-secret-ns` erfolgreich erstellt mit den spezifizierten Zugangsdaten.
- Ein Pod `ckad-secret-pod` ist korrekt erstellt und nutzt das Secret, um die Zugangsdaten als Umgebungsvariablen bereitzustellen.
- Die Zugangsdaten sind im Container des Pods verfügbar und korrekt als Umgebungsvariablen gesetzt.
