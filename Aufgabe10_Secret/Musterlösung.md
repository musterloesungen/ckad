## Musterlösung: Einrichtung und Nutzung eines Kubernetes Secrets

**Schritt 1: Erstellung eines Kubernetes Secrets**

Erstelle ein Secret `ckad-secret.yaml` im Namespace `ckad-secret-ns`:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: ckad-secret
  namespace: ckad-secret-ns
type: Opaque
data:
  username: Y2thZHVzZXI=  # Base64-encoded string 'ckaduser'
  password: Y2thZHBhc3M=  # Base64-encoded string 'ckadpass'
```

Wende das Secret an:

```bash
kubectl apply -f ckad-secret.yaml
```

**Schritt 2: Nutzung des Secrets in einem Pod**

Erstelle einen Pod `ckad-secret-pod.yaml`, der das Secret als Umgebungsvariablen nutzt:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ckad-secret-pod
  namespace: ckad-secret-ns
spec:
  containers:
  - name: busybox
    image: busybox
    command: [ "sh", "-c", "sleep 3600" ]
    env:
      - name: USERNAME
        valueFrom:
          secretKeyRef:
            name: ckad-secret
            key: username
      - name: PASSWORD
        valueFrom:
          secretKeyRef:
            name: ckad-secret
            key: password
```

Wende den Pod an:

```bash
kubectl apply -f ckad-secret-pod.yaml
```

**Überprüfung:**

Um zu überprüfen, ob die Umgebungsvariablen korrekt im Container gesetzt sind:

```bash
kubectl exec -n ckad-secret-ns ckad-secret-pod -- env | grep USERNAME
kubectl exec -n ckad-secret-ns ckad-secret-pod -- env | grep PASSWORD
```

**Erwartetes Ergebnis:**
- Das Secret `ckad-secret` ist im Namespace `ckad-secret-ns` erfolgreich erstellt.
- Der Pod `ckad-secret-pod` nutzt das Secret, um die Zugangsdaten als Umgebungsvariablen bereitzustellen.
- Die Umgebungsvariablen `USERNAME` und `PASSWORD` im Container des Pods enthalten die korrekten Werte aus dem Secret.
