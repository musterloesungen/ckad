## Übungsaufgabe für CKAD Prüfung: SecurityContext Problemanalyse

**Ziel:** Analyse und Behebung eines Berechtigungsproblems in einem Kubernetes Container, der nicht wie erwartet funktioniert.

**Aufgabenstellung:**

1. **Ausrollen des bereitgestellten Manifests**
    - Verwende das untenstehende Kubernetes Manifest `problematisches-deployment.yaml`, um ein Deployment zu erstellen.
    - Das Deployment beinhaltet einen Container, der für einen bestimmten Zweck konfiguriert ist.

2. **Fehlerbeobachtung und -analyse**
    - Nachdem das Deployment ausgeführt wurde, überprüfe den Status des Pods.
    - Untersuche die Logs des Containers, um festzustellen, ob und welche Probleme auftreten.

3. **Problemidentifikation**
    - Analysiere, warum der Container nicht wie erwartet funktioniert.
    - Überlege, welche Berechtigungen der Container benötigen könnte und wie der aktuelle SecurityContext konfiguriert ist.

4. **Lösungsvorschlag**
    - Formuliere basierend auf deiner Analyse einen Lösungsvorschlag, um das Problem zu beheben.
    - Modifiziere das Deployment entsprechend deines Lösungsvorschlags und wende die Änderungen an.

**Bereitgestelltes Manifest: `problematisches-deployment.yaml`**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: berechtigungsproblem-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: problem-app
  template:
    metadata:
      labels:
        app: problem-app
    spec:
      containers:
      - name: problem-container
        image: nginx
        securityContext:
          runAsUser: 1000
        ports:
        - containerPort: 80
```

**Erfolgskriterien:**
- Identifikation des Problems im Zusammenhang mit dem SecurityContext und den Berechtigungen des Containers
- Formulierung einer effektiven Lösung, um das Berechtigungsproblem zu beheben
- Erfolgreiche Ausführung des Containers im Deployment nach Anwendung der Lösung
