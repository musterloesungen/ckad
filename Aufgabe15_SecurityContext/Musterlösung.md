## Musterlösung: SecurityContext Problemanalyse und Behebung

**Problemidentifikation:**
Das bereitgestellte Manifest `problematisches-deployment.yaml` konfiguriert den Container (`problem-container`) so, dass er mit der Benutzer-ID 1000 ausgeführt wird, anstatt als Root-Benutzer. Da nginx standardmäßig versucht, auf Ports unter 1024 zu binden und bestimmte Operationen auszuführen, die Root-Rechte erfordern, kann es zu Problemen kommen, wenn es als ein eingeschränkter Benutzer ausgeführt wird.

**Schritte zur Problemlösung:**

1. **Überprüfung des Pod-Status und Container-Logs**
    - Befehle:
        - `kubectl get pods -l app=problem-app`
        - `kubectl logs <pod-name>`

   Bei der Überprüfung der Logs wird wahrscheinlich eine Fehlermeldung angezeigt, die darauf hindeutet, dass nginx nicht die erforderlichen Berechtigungen hat, um bestimmte Operationen durchzuführen.

2. **Anpassung des Manifests**
    - Um das Problem zu beheben, kann man entweder den Container so konfigurieren, dass er als Root-Benutzer läuft oder nginx so anpassen, dass es keine Root-Rechte benötigt.
    - In diesem Fall passen wir nginx an, um auf einem höheren Port zu laufen und somit die Notwendigkeit von Root-Rechten zu umgehen.

**Modifiziertes Manifest: `angepasstes-deployment.yaml`**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: berechtigungsproblem-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: problem-app
  template:
    metadata:
      labels:
        app: problem-app
    spec:
      containers:
      - name: problem-container
        image: nginx
        securityContext:
          runAsUser: 1000
        ports:
        - containerPort: 8080
        command: ["/usr/sbin/nginx"]
        args: ["-g", "daemon off;", "-c", "/etc/nginx/nginx.conf"]
```

**Änderungen:**
- Der Container wird weiterhin als Benutzer mit der ID 1000 ausgeführt.
- nginx wird so konfiguriert, dass es auf Port 8080 läuft, ein Port, der keine Root-Rechte erfordert.

**Anwenden der Änderungen:**
- Wende das aktualisierte Manifest an:
    - `kubectl apply -f angepasstes-deployment.yaml`

**Überprüfung:**
- Nachdem das aktualisierte Deployment angewendet wurde, überprüfe erneut den Status der Pods und die Container-Logs, um sicherzustellen, dass nginx jetzt erfolgreich startet und läuft.
