## Musterlösung: YAML-Definitionen für Blue-Green Deployment

### Namespace Manifest

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-blue-green-ns
```

### Blue Deployment Manifest

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-blue
  namespace: ckad-blue-green-ns
spec:
  replicas: 3
  selector:
    matchLabels:
      app: ckad-blue-green
      version: blue
  template:
    metadata:
      labels:
        app: ckad-blue-green
        version: blue
    spec:
      containers:
      - name: nginx
        image: nginx:1.17
        ports:
        - containerPort: 80
```

### Green Deployment Manifest

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-green
  namespace: ckad-blue-green-ns
spec:
  replicas: 3
  selector:
    matchLabels:
      app: ckad-blue-green
      version: green
  template:
    metadata:
      labels:
        app: ckad-blue-green
        version: green
    spec:
      containers:
      - name: nginx
        image: nginx:1.25
        ports:
        - containerPort: 80
```

### Service Manifest

```yaml
apiVersion: v1
kind: Service
metadata:
  name: ckad-service
  namespace: ckad-blue-green-ns
spec:
  selector:
    app: ckad-blue-green
    version: blue
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
```

### Anweisungen zur Anwendung und Überprüfung des Blue-Green Deployments

1. Erstelle den Namespace `ckad-blue-green-ns`:

   ```bash
   kubectl apply -f namespace-manifest.yaml
   ```

2. Wende das Blue Deployment Manifest an:

   ```bash
   kubectl apply -f blue-deployment.yaml
   ```

3. Wende das Service Manifest an:

   ```bash
   kubectl apply -f service.yaml
   ```

4. Überprüfe den Status des Blue Deployments:

   ```bash
   kubectl get deployments -n ckad-blue-green-ns
   ```

5. Starte das Green Deployment:

   ```bash
   kubectl apply -f green-deployment.yaml
   ```

6. Überprüfe den Status des Green Deployments:

   ```bash
   kubectl get deployments -n ckad-blue-green-ns
   ```

7. Schalte den Service um, sodass er auf das Green Deployment zeigt:

   - Aktualisiere den Service Selector, um auf die Green Version zu zeigen:

   ```yaml
   spec:
     selector:
       app: ckad-blue-green
       version: green
   ```

   - Wende die Änderung an:

   ```bash
   kubectl apply -f service.yaml
   ```

8. Verifiziere, dass der Service nun die Green Pods bedient:

   ```bash
   kubectl describe service ckad-service -n ckad-blue-green-ns
   ```

   Überprüfe die `Endpoints` Sektion in der Beschreibung, um sicherzustellen, dass die IPs zu den Green Pods gehören.
