## CKAD Trainingsaufgabe: Implementierung eines Blue-Green-Deployments

1. Erstelle zwei Deployments im Namespace `ckad-blue-green-ns`:
   - Das **Blue Deployment** soll den Namen `ckad-blue` tragen und das Image `nginx:1.17` verwenden.
   - Das **Green Deployment** soll den Namen `ckad-green` tragen und das Image `nginx:1.25` verwenden.
2. Beide Deployments sollen jeweils drei Replikate haben.
3. Konfiguriere eine Service-Ressource namens `ckad-service`:
   - Der Service soll anfangs auf das Blue Deployment zeigen.
   - Der Service soll auf Port `80` lauschen und an die entsprechenden Ports der Pods weiterleiten.
4. Nachdem das Blue Deployment läuft und vom Service genutzt wird, starte das Green Deployment.
5. Stelle sicher, dass das Green Deployment erfolgreich gestartet ist und die Pods betriebsbereit sind.
6. Schalte den Service um, sodass er auf das Green Deployment zeigt, ohne Ausfallzeit für die Nutzer.
7. Verifiziere, dass der Service nun die Pods des Green Deployments bedient.
8. Der Namespace `ckad-blue-green-ns` soll erstellt werden, falls er nicht existiert.
