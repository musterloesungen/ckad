## Übungsaufgabe für CKAD Prüfung: Aktualisierung eines veralteten Manifests auf Kubernetes 1.28

**Ziel:** Aktualisieren eines Kubernetes-Manifests, das eine veraltete API-Version verwendet, auf die aktuelle Version 1.28.

**Ausgangssituation:**
Ein veraltetes Kubernetes-Manifest soll auf die neueste Version aktualisiert werden.

**Veraltetes Manifest: `veraltetes-deployment.yaml`**

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mein-altes-deployment
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: mein-altes-app
    spec:
      containers:
      - name: nginx
        image: nginx:1.16
```

In diesem Beispiel verwendet das Deployment die veraltete API-Version `extensions/v1beta1`, die in Kubernetes 1.28 nicht mehr unterstützt wird.

**Aufgabenstellung:**

1. **Analyse des veralteten Manifests**
    - Untersuche das bereitgestellte veraltete Manifest und identifiziere die veraltete API-Version.

2. **Recherche der aktuellen API-Version**
    - Ermittle die aktuelle API-Version für Deployments in Kubernetes 1.28. Verwende dazu die offizielle Kubernetes-Dokumentation oder den `kubectl explain` Befehl.

3. **Aktualisierung des Manifests**
    - Aktualisiere das Manifest, um die korrekte, unterstützte API-Version zu verwenden. Stelle sicher, dass alle anderen notwendigen Anpassungen vorgenommen werden, damit das Manifest mit Kubernetes 1.28 kompatibel ist.

4. **Testen des aktualisierten Manifests**
    - Wende das aktualisierte Manifest im Cluster an und überprüfe, ob das Deployment korrekt erstellt wird und funktioniert.

**Erfolgskriterien:**
- Das Manifest `veraltetes-deployment.yaml` ist erfolgreich auf die aktuelle API-Version für Kubernetes 1.28 aktualisiert.
- Das aktualisierte Deployment wird ohne Fehler im Cluster erstellt und ist funktionsfähig.
