## Musterlösung: Aktualisierung eines veralteten Manifests auf Kubernetes 1.28

**Schritt 1: Analyse des veralteten Manifests**

Das bereitgestellte Manifest `veraltetes-deployment.yaml` verwendet die veraltete API-Version `extensions/v1beta1` für ein Deployment. Diese API-Version wird in Kubernetes 1.28 nicht mehr unterstützt.

**Veraltetes Manifest: `veraltetes-deployment.yaml`**

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mein-altes-deployment
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: mein-altes-app
    spec:
      containers:
      - name: nginx
        image: nginx:1.16
```

**Schritt 2: Recherche der aktuellen API-Version**

Die aktuelle API-Version für Deployments in Kubernetes 1.28 ist `apps/v1`.

**Schritt 3: Aktualisierung des Manifests**

Aktualisiere das Manifest, um die neue API-Version zu verwenden und stelle sicher, dass alle Felder korrekt sind.

**Aktualisiertes Manifest: `aktualisiertes-deployment.yaml`**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mein-neues-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: mein-neues-app
  template:
    metadata:
      labels:
        app: mein-neues-app
    spec:
      containers:
      - name: nginx
        image: nginx:1.16
```

**Schritt 4: Testen des aktualisierten Manifests**

Wende das aktualisierte Manifest im Cluster an:

```bash
kubectl apply -f aktualisiertes-deployment.yaml
```

Überprüfe dann, ob das Deployment korrekt erstellt wurde:

```bash
kubectl get deployment mein-neues-deployment
```

**Erwartetes Ergebnis:**
- Das aktualisierte Manifest `aktualisiertes-deployment.yaml` verwendet die aktuelle API-Version `apps/v1`.
- Das Deployment wird erfolgreich im Kubernetes Cluster erstellt und läuft wie erwartet.
