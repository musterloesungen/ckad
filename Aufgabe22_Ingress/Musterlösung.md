## Musterlösung: Einrichtung eines Ingress

### Schritt 1: Erstellen eines einfachen Web-Services

**1.1 Deployment erstellen**

Erstelle ein YAML-Manifest `web-server-deployment.yaml` für einen einfachen Nginx-Webserver:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: web-server
  template:
    metadata:
      labels:
        app: web-server
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80
```

Wende das Manifest an:

```bash
kubectl apply -f web-server-deployment.yaml
```

**1.2 Service erstellen**

Erstelle einen Service `web-server-service.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: web-server
spec:
  selector:
    app: web-server
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

Wende den Service an:

```bash
kubectl apply -f web-server-service.yaml
```

### Schritt 2: Einrichten einer Ingress-Ressource

Erstelle eine Ingress-Ressource `web-server-ingress.yaml`:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: web-server-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: k8s.biederbeck.de
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: web-server
            port:
              number: 80
```

Wende die Ingress-Ressource an:

```bash
kubectl apply -f web-server-ingress.yaml
```

### Schritt 3: Testen des Ingress

- Verwende einen Browser oder einen HTTP-Client wie `curl` um die Funktionalität zu testen:

  ```bash
  curl http://k8s.biederbeck.de
  ```

### Schritt 4: Einrichtung von HTTPS (Optional)

- Erstelle und wende ein SSL/TLS-Zertifikat an (selbstsigniert oder von einer Zertifizierungsstelle).
- Aktualisiere die Ingress-Ressource, um auf das Zertifikat zu verweisen.

### Ergebnis:

- Der Nginx-Webserver ist als Deployment mit einem dazugehörigen Service eingerichtet.
- Die Ingress-Ressource leitet Anfragen an `k8s.biederbeck.de` korrekt an den Webserver-Service weiter.
- Optional: HTTPS ist konfiguriert und funktioniert entsprechend.
