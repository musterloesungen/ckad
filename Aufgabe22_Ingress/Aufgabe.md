## Übungsaufgabe für CKAD Prüfung: Einrichtung eines Ingress

**Ziel:** Einrichten und Testen eines Ingress-Controllers und einer Ingress-Ressource unter Verwendung der Domain `k8s.biederbeck.de`.

**Aufgabenstellung:**

1. **Erstellen eines einfachen Web-Services**
    - Erstelle ein Deployment, das einen einfachen Webserver (z.B. Nginx oder HTTPd) ausführt.
    - Erstelle einen Service, der dieses Deployment abbildet.

2. **Einrichten einer Ingress-Ressource**
    - Konfiguriere eine Ingress-Ressource für den oben erstellten Service.
    - Die Ingress-Ressource soll Anfragen an `k8s.biederbeck.de` an den Service weiterleiten.

3. **Testen des Ingress**
    - Überprüfe, ob der Ingress korrekt eingerichtet ist und Anfragen an den Web-Service weiterleitet.
    - Teste den Zugriff auf den Web-Service über einen Browser oder einen HTTP-Client wie `curl` unter Verwendung der Domain `k8s.biederbeck.de`.

4. **Sicherheitsaspekte**
    - Optional: Konfiguriere HTTPS für den Ingress, indem du ein SSL/TLS-Zertifikat verwendest. Du kannst ein selbstsigniertes Zertifikat verwenden oder ein echtes Zertifikat, wenn verfügbar.

**Hinweise:**
- Stelle sicher, dass der Ingress-Controller im Cluster korrekt funktioniert.
- Verwende korrekte Annotations in der Ingress-Ressource, um spezifische Einstellungen für deinen Ingress-Controller festzulegen.
- Beachte, dass DNS-Einstellungen und die Verfügbarkeit der Domain `k8s.biederbeck.de` für den externen Zugriff benötigt werden.

**Erfolgskriterien:**
- Ein Web-Service-Deployment und der zugehörige Service sind erfolgreich erstellt.
- Eine Ingress-Ressource ist korrekt konfiguriert und leitet Anfragen an die Domain `k8s.biederbeck.de` an den Web-Service weiter.
- Der Zugriff auf den Web-Service über die angegebene Domain funktioniert wie erwartet.
- Optional: HTTPS ist auf dem Ingress konfiguriert und funktioniert korrekt.
