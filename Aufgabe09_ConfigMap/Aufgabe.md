## Übungsaufgabe für CKAD Prüfung: Erstellung und Einsatz einer ConfigMap

**Ziel:** Konfiguration eines Pods in Kubernetes mit einer ConfigMap.

**Aufgabenstellung:**

1. **Erstellen einer ConfigMap**
    - Erstelle eine ConfigMap mit dem Namen `ckad-config` im Namespace `ckad-config-ns`.
    - Die ConfigMap soll zwei Datenpunkte enthalten: Ein Feld namens `config.json` mit einem JSON-Objekt deiner Wahl und ein Feld namens `loglevel` mit dem Wert `info`.

2. **Einbindung der ConfigMap in einen Pod**
    - Erstelle einen Pod mit dem Namen `ckad-config-pod`.
    - Der Pod soll einen Container beinhalten, der das `busybox` Image nutzt.
    - Der Container soll die zuvor erstellte ConfigMap auf zwei Arten nutzen:
        - Als Umgebungsvariablen
        - Als eingebundenes Volume

**Hinweise:**
- Achte darauf, wie die ConfigMap im Pod referenziert wird, sowohl für Umgebungsvariablen als auch als Volume.
- Überprüfe die korrekte Erstellung und Einbindung der ConfigMap, um sicherzustellen, dass die Konfigurationsdaten im Pod verfügbar sind.

**Erfolgskriterien:**
- Eine ConfigMap `ckad-config` ist im Namespace `ckad-config-ns` erfolgreich erstellt mit den spezifizierten Daten.
- Ein Pod `ckad-config-pod` ist korrekt erstellt, der die ConfigMap sowohl als Umgebungsvariablen als auch als Volume nutzt.
- Die Konfiguration innerhalb des Pods reflektiert die in der ConfigMap definierten Werte.
