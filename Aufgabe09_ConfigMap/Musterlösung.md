## Musterlösung: Erstellung und Einsatz einer ConfigMap

**Schritt 1: Erstellen einer ConfigMap**

Erstelle eine ConfigMap `ckad-config.yaml` im Namespace `ckad-config-ns`:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: ckad-config
  namespace: ckad-config-ns
data:
  config.json: |-
    {
      "key": "value"
    }
  loglevel: "info"
```

Wende die ConfigMap an:

```bash
kubectl apply -f ckad-config.yaml
```

**Schritt 2: Einbindung der ConfigMap in einen Pod**

Erstelle einen Pod `ckad-config-pod.yaml`, der die ConfigMap nutzt:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ckad-config-pod
  namespace: ckad-config-ns
spec:
  containers:
  - name: busybox
    image: busybox
    command: [ "sh", "-c", "sleep 3600" ]
    env:
      - name: LOG_LEVEL
        valueFrom:
          configMapKeyRef:
            name: ckad-config
            key: loglevel
    volumeMounts:
    - name: config-volume
      mountPath: /config
  volumes:
    - name: config-volume
      configMap:
        name: ckad-config
        items:
        - key: config.json
          path: config.json
```

Wende den Pod an:

```bash
kubectl apply -f ckad-config-pod.yaml
```

**Überprüfung:**

1. **Überprüfung der Umgebungsvariablen:**

   Um zu überprüfen, ob die Umgebungsvariable im Container gesetzt ist:

   ```bash
   kubectl exec -n ckad-config-ns ckad-config-pod -- env | grep LOG_LEVEL
   ```

2. **Überprüfung des Volumes:**

   Um zu überprüfen, ob das Volume korrekt eingebunden wurde und die Datei `config.json` enthält:

   ```bash
   kubectl exec -n ckad-config-ns ckad-config-pod -- cat /config/config.json
   ```

**Erwartetes Ergebnis:**
- Die ConfigMap `ckad-config` ist im Namespace `ckad-config-ns` erfolgreich erstellt.
- Der Pod `ckad-config-pod` ist korrekt erstellt und bindet die ConfigMap sowohl als Umgebungsvariable (`LOG_LEVEL`) als auch als Volume (`config.json`) ein.
- Die Daten aus der ConfigMap sind im Pod verfügbar und können entsprechend verwendet werden.
