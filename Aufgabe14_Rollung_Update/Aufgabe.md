## Übungsaufgabe für CKAD Prüfung: Deployment mit Rolling Update

**Ziel:** Erstellen und Aktualisieren eines Kubernetes Deployments mit der Rolling Update Strategie.

**Aufgabenstellung:**

1. **Erstellen eines neuen Deployments**
    - Name des Deployments: `mein-app-deployment`
    - Verwende das Image `nginx:1.16` für den Pod
    - Stelle sicher, dass 3 Replikate des Pods laufen
    - Definiere Readiness und Liveness Probes, die den HTTP-Port 80 überprüfen

2. **Verifizieren des Deployments**
    - Überprüfe den Status des Deployments
    - Stelle sicher, dass alle Pods erfolgreich gestartet sind

3. **Durchführen eines Rolling Updates**
    - Aktualisiere das Deployment auf das Image `nginx:1.17`
    - Wende die Änderungen an, ohne Downtime zu verursachen

4. **Überwachen des Update-Prozesses**
    - Überwache den Fortschritt des Rolling Updates
    - Stelle sicher, dass während des Updates immer eine angegebene Mindestanzahl von Pods verfügbar ist

5. **Validieren des Updates**
    - Überprüfe, ob das Update erfolgreich war und alle Pods das neue Image verwenden
    - Teste die Verfügbarkeit der Anwendung während und nach dem Update

**Hinweise:**
- Verwende kubectl-Befehle, um das Deployment zu erstellen und zu aktualisieren
- Achte auf korrekte Yaml-Konfigurationen für Deployments und Probes

**Zusätzliche Herausforderung:**
- Versuche, ein Rollback auf die vorherige Version durchzuführen, falls das Update fehlschlägt

**Erfolgskriterien:**
- Das Deployment ist erfolgreich mit 3 Replikaten des nginx:1.16 Images erstellt
- Alle Pods sind betriebsbereit und passieren Readiness/Liveness Probes
- Das Rolling Update auf nginx:1.17 wird ohne Downtime durchgeführt
- Nach dem Update laufen alle Pods mit dem neuen Image
- Das Deployment bleibt während des gesamten Prozesses verfügbar
