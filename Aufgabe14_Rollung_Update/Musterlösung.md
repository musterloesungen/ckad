## Musterlösung: Deployment mit Rolling Update in Kubernetes

**Schritt 1: Erstellen des Deployments**

Erstelle eine Yaml-Konfigurationsdatei `mein-app-deployment.yaml` mit folgendem Inhalt:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mein-app-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: meine-app
  template:
    metadata:
      labels:
        app: meine-app
    spec:
      containers:
      - name: nginx
        image: nginx:1.16
        ports:
        - containerPort: 80
        readinessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 5
          periodSeconds: 5
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 15
          periodSeconds: 20
```

Führe den Befehl aus, um das Deployment zu erstellen:

```bash
kubectl apply -f mein-app-deployment.yaml
```

**Schritt 2: Überprüfen des Deployments**

Stelle sicher, dass das Deployment erfolgreich erstellt wurde:

```bash
kubectl get deployments
```

Überprüfe den Status der Pods:

```bash
kubectl get pods
```

**Schritt 3: Durchführen des Rolling Updates**

Aktualisiere das Deployment auf das neue Image:

```bash
kubectl set image deployment/mein-app-deployment nginx=nginx:1.17
```

**Schritt 4: Überwachung des Update-Prozesses**

Beobachte den Rollout-Status, um den Fortschritt des Updates zu überwachen:

```bash
kubectl rollout status deployment/mein-app-deployment
```

**Schritt 5: Validierung des Updates**

Überprüfe, ob alle Pods das neue Image verwenden:

```bash
kubectl get pods -l app=meine-app -o jsonpath="{..image}"
```

Stelle sicher, dass das Deployment erfolgreich aktualisiert wurde:

```bash
kubectl describe deployment mein-app-deployment
```

**Rollback bei Fehlschlag (Zusätzliche Herausforderung)**

Wenn das Update fehlschlägt, führe einen Rollback auf die vorherige Version durch:

```bash
kubectl rollout undo deployment/mein-app-deployment
```
