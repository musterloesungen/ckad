## Musterlösung: Erstellung und Zuweisung von Rollen und RoleBindings

**Schritt 1: Erstellen einer Rolle**

Erstelle eine Yaml-Datei `role-pod-reader.yaml` für die Rolle `pod-reader`:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: pod-reader
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
```

Wende die Rolle an:

```bash
kubectl apply -f role-pod-reader.yaml
```

**Schritt 2: Erstellen eines Service Accounts**

Erstelle einen Service Account mit dem Namen `mein-service-account` im Namespace `default`:

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: mein-service-account
  namespace: default
```

Wende den Service Account an:

```bash
kubectl apply -f service-account.yaml
```

**Schritt 3: Erstellen eines RoleBindings**

Erstelle ein RoleBinding, um die Rolle `pod-reader` mit dem Service Account `mein-service-account` zu verknüpfen:

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: pod-reader-binding
  namespace: default
subjects:
- kind: ServiceAccount
  name: mein-service-account
  namespace: default
roleRef:
  kind: Role
  name: pod-reader
  apiGroup: rbac.authorization.k8s.io
```

Wende das RoleBinding an:

```bash
kubectl apply -f rolebinding.yaml
```

**Schritt 4: Testen der Konfiguration**

Erstelle einen Test-Pod, der den `mein-service-account` verwendet, um zu überprüfen, ob die Berechtigungen korrekt funktionieren. Hier ist ein Beispiel für einen solchen Pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pod
  namespace: default
spec:
  serviceAccountName: mein-service-account
  containers:
  - name: test-container
    image: bitnami/kubectl
    command: ['sh', '-c', 'echo Container is Running; sleep 3600']
```

Wende den Pod an:

```bash
kubectl apply -f test-pod.yaml
```

Nachdem der Pod gestartet ist, führe eine Remote-Shell in ihm aus und versuche, mit `kubectl get pods` die Pods aufzulisten:

```bash
kubectl exec -it test-pod -- /bin/bash
kubectl get pods
```

**Erwartetes Ergebnis:**
- Der Befehl im Pod sollte erfolgreich die Liste der Pods im `default` Namespace anzeigen, was zeigt, dass die Rolle `pod-reader` korrekt funktioniert.
