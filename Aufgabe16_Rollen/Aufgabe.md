## Übungsaufgabe für CKAD Prüfung: Erstellung und Zuweisung von Rollen und RoleBindings

**Ziel:** Erfolgreiche Erstellung und Zuweisung von Rollen und RoleBindings in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Erstellen einer Rolle**
    - Erstelle eine Rolle namens `pod-reader` im Namespace `default`.
    - Die Rolle soll Berechtigungen haben, um `get`, `watch` und `list` Operationen auf Pods im `default` Namespace auszuführen.

2. **Erstellen eines Service Accounts**
    - Erstelle einen Service Account mit dem Namen `mein-service-account` im selben Namespace.

3. **Erstellen eines RoleBindings**
    - Erstelle ein RoleBinding, um die Rolle `pod-reader` dem Service Account `mein-service-account` zuzuweisen.

4. **Testen der Konfiguration**
    - Teste die Konfiguration, indem du einen Pod erstellst, der den `mein-service-account` verwendet.
    - Führe in diesem Pod Befehle aus, um die Berechtigungen zu testen (z.B. `kubectl get pods`).

**Hinweise:**
- Beachte die korrekte Syntax und Struktur für Rollen, Service Accounts und RoleBindings in Kubernetes.
- RBAC in Kubernetes dient dazu, den Zugriff auf Ressourcen fein abzustimmen.

**Erfolgskriterien:**
- Die Rolle `pod-reader` ist korrekt mit den erforderlichen Berechtigungen erstellt.
- Der Service Account `mein-service-account` ist erfolgreich erstellt und dem RoleBinding zugewiesen.
- Das RoleBinding verknüpft die Rolle und den Service Account korrekt.
- Die Zugriffsberechtigungen werden erfolgreich im Test-Pod mit dem Service Account überprüft.
