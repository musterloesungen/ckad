## Musterlösung: Erstellen eines Custom Resource Definitions (CRD)

**Schritt 1: Entwicklung eines CRD**

Erstelle ein Custom Resource Definition (CRD) für die Ressource `MeinCustomResource`. Hier ist ein Beispiel für eine solche Definition:

```yaml
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  name: meincustomresources.beispiel.com
spec:
  group: beispiel.com
  versions:
    - name: v1
      served: true
      storage: true
      schema:
        openAPIV3Schema:
          type: object
          properties:
            spec:
              type: object
              properties:
                name:
                  type: string
                size:
                  type: integer
  scope: Namespaced
  names:
    plural: meincustomresources
    singular: meincustomresource
    kind: MeinCustomResource
    shortNames:
    - mcr
```

**Schritt 2: Erstellen des CRD im Cluster**

Wende das erstellte CRD im Kubernetes Cluster an:

```bash
kubectl apply -f meincustomresource-crd.yaml
```

**Schritt 3: Erstellen einer Instanz der Custom Resource**

Erstelle eine Instanz der Custom Resource `MeinCustomResource`. Hier ist ein Beispiel:

```yaml
apiVersion: beispiel.com/v1
kind: MeinCustomResource
metadata:
  name: beispiel-resource
spec:
  name: "BeispielName"
  size: 3
```

Wende die Custom Resource im Cluster an:

```bash
kubectl apply -f beispiel-resource.yaml
```

**Schritt 4: Überprüfung der Custom Resource**

Überprüfe, ob die Custom Resource erfolgreich im Cluster erstellt wurde:

```bash
kubectl get meincustomresources.beispiel.com
kubectl describe meincustomresource beispiel-resource
```

**Erwartetes Ergebnis:**
- Das CRD `MeinCustomResource` ist im Cluster sichtbar und korrekt definiert.
- Die Instanz `beispiel-resource` der Custom Resource `MeinCustomResource` ist korrekt erstellt, mit den spezifizierten Feldern `spec.name` und `spec.size`.
