## Übungsaufgabe für CKAD Prüfung: Erstellen eines Custom Resource Definitions (CRD)

**Ziel:** Entwicklung und Anwendung eines Custom Resource Definitions (CRD) in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Entwicklung eines CRD**
    - Entwickle ein CRD für eine Ressource namens `MeinCustomResource`.
    - Definiere im CRD mindestens zwei spezifische Felder: `spec.name` (Typ: String) und `spec.size` (Typ: Integer).

2. **Erstellen des CRD im Cluster**
    - Wende das erstellte CRD im Kubernetes Cluster an.

3. **Erstellen einer Instanz der Custom Resource**
    - Erstelle eine Instanz der Custom Resource `MeinCustomResource` mit Werten für `spec.name` und `spec.size`.

4. **Überprüfung der Custom Resource**
    - Überprüfe, ob die Custom Resource erfolgreich im Cluster erstellt wurde und ob die definierten Felder korrekt sind.

**Hinweise:**
- Beachte die korrekte Struktur und Syntax für das CRD.
- Stelle sicher, dass das CRD und die Custom Resource korrekt im Kubernetes API Server registriert sind.

**Erfolgskriterien:**
- Das CRD `MeinCustomResource` ist erfolgreich im Kubernetes Cluster erstellt.
- Eine Instanz der Custom Resource `MeinCustomResource` ist erfolgreich erstellt mit den spezifizierten Feldern `spec.name` und `spec.size`.
- Die Custom Resource ist im Cluster sichtbar und die Felder sind korrekt definiert.
