## Musterlösung: Erstellung und Installation eines lokalen Helm Charts für `myapp`

### Helm Chart Erstellung

1. Erstelle ein neues Helm Chart für `myapp`:

```bash
helm create myapp
```

2. Bearbeite die `values.yaml` Datei des Charts, um die Standardwerte festzulegen:

```yaml
replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  tag: "latest"

service:
  type: LoadBalancer
  port: 80
```

3. Bearbeite die `deployment.yaml` im Verzeichnis `templates`, um Liveness und Readiness Probes hinzuzufügen:

```yaml
livenessProbe:
  httpGet:
    path: /
    port: http
readinessProbe:
  httpGet:
    path: /
    port: http
```

4. Bearbeite die `service.yaml` im Verzeichnis `templates`, um den Service vom Typ `LoadBalancer` zu konfigurieren:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ include "myapp.fullname" . }}
spec:
  type: {{ .Values.service.type }}
  ports:
  - port: {{ .Values.service.port }}
    targetPort: http
    protocol: TCP
    name: http
  selector:
    {{- include "myapp.selectorLabels" . | nindent 4 }}
```

5. Füge eine README-Datei zum Chart hinzu, die Anweisungen zur Installation und Konfiguration enthält.

### Helm Chart Installation

1. Installiere das Helm Chart `myapp` im Namespace `ckad-helm-ns`. Da wir kein Helm Repository haben, verwenden wir den lokalen Pfad zum Chart:

```bash
helm install myapp-release ./myapp --namespace ckad-helm-ns --create-namespace --kubeconfig /path/to/kubeconfig
```

Ersetze `/path/to/kubeconfig` mit dem Pfad zur tatsächlichen Kubeconfig-Datei.

2. Überprüfe den Status des Deployments und des Services, um sicherzustellen, dass die Anwendung korrekt installiert wurde:

```bash
kubectl get deployments,services -n ckad-helm-ns --kubeconfig /path/to/kubeconfig
```

### Hinweise zur Überprüfung der Installation

- Der Status des Deployments sollte `READY` sein, um zu zeigen, dass die Pods laufen.
- Der Service sollte eine externe IP (abhängig vom Cloud-Provider oder der Netzwerkkonfiguration) zeigen, die auf Port `80` erreichbar ist.
