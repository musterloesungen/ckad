## Musterlösung: Erstellung eines Helm Charts für `myapp`

### Struktur des Helm Charts

```
myapp/
|-- Chart.yaml
|-- values.yaml
|-- charts/
|-- templates/
|   |-- deployment.yaml
|   |-- service.yaml
|-- README.md
```

### Chart.yaml

```yaml
apiVersion: v2
name: myapp
description: A Helm chart for Kubernetes

type: application

version: 0.1.0
appVersion: "1.0"
```

### values.yaml

```yaml
replicaCount: 1

image:
  repository: nginx
  pullPolicy: IfNotPresent
  tag: "latest"

service:
  type: LoadBalancer
  port: 80
```

### templates/deployment.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "myapp.fullname" . }}
  labels:
    {{- include "myapp.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "myapp.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "myapp.selectorLabels" . | nindent 8 }}
    spec:
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          ports:
            - name: http
              containerPort: {{ .Values.service.port }}
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
```

### templates/service.yaml

```yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ include "myapp.fullname" . }}
  labels:
    {{- include "myapp.labels" . | nindent 4 }}
spec:
  type: {{ .Values.service.type }}
  ports:
    - port: {{ .Values.service.port }}
      targetPort: http
      protocol: TCP
      name: http
  selector:
    {{- include "myapp.selectorLabels" . | nindent 4 }}
```

### README.md

```markdown
# Myapp

This is a Helm chart for deploying `nginx` on Kubernetes.

## Installation

To install the chart with the release name `my-release`:

```bash
helm repo add myapp-repo [URL to your Helm repository]
helm install my-release myapp-repo/myapp
```

## Configuration

The following table lists the configurable parameters of the `myapp` chart and their default values.

| Parameter           | Description             | Default        |
|---------------------|-------------------------|----------------|
| `replicaCount`      | Number of replicas      | `1`            |
| `image.repository`  | Image repository        | `nginx`        |
| `image.pullPolicy`  | Image pull policy       | `IfNotPresent` |
| `image.tag`         | Image tag               | `latest`       |
| `service.type`      | Service type            | `LoadBalancer` |
| `service.port`      | Service port            | `80`           |

To override the default values, you can use the `--set` flag with `helm install`, for example:

```bash
helm install my-release myapp-repo/myapp --set service.port=8080
```

### Verpacken und Hochladen des Charts

1. Verpacke das Helm Chart:

   ```bash
   helm package myapp
   ```

2. Lade das verpackte Chart in dein Helm Repository hoch:

   ```bash
   helm repo add myapp-repo [URL to your Helm repository]
   helm push myapp-0.1.0.tgz myapp-repo
   ```

### Installation des Charts

1. Führe die Helm-Installation des Charts im Namespace `ckad-helm-ns` aus:

   ```bash
   helm install myapp-release myapp-repo/myapp --namespace ckad-helm-ns --create-namespace
   ```

2. Überprüfe den Status der Anwendung:

   ```bash
   kubectl get all -n ckad-helm-ns
   ```

3. Überprüfe die Erreichbarkeit des Services (abhängig von deiner Kubernetes-Installation und den Netzwerkeinstellungen).
