## CKAD Trainingsaufgabe: Erstellung eines Helm Charts

1. Erstelle ein Helm Chart mit dem Namen `myapp`.
2. Das Chart soll zwei Kubernetes Ressourcen definieren: ein Deployment und einen Service.
3. Das Deployment soll das Image `nginx` verwenden und auf Port `80` laufen.
4. Der Service soll vom Typ `LoadBalancer` sein und das Deployment auf Port `80` zugänglich machen.
5. Füge dem Helm Chart Template-Dateien hinzu, um die Konfiguration des Images und des Ports über die `values.yaml` Datei anpassbar zu machen.
6. In der `values.yaml` Datei, setze Standardwerte für das Image auf `nginx:latest` und den Port auf `80`.
7. Stelle sicher, dass das Helm Chart einen Health Check für die Deployments via Liveness und Readiness Probes beinhaltet.
8. Füge dem Chart eine README-Datei hinzu, die Anweisungen zur Installation des Charts und zur Anpassung der Standardwerte enthält.
9. Verpacke das Chart und lade es in ein Helm Repository hoch.
10. Führe eine Helm-Installation des Charts in einem Namespace `ckad-helm-ns` aus und überprüfe, ob die Anwendung erreichbar ist.
