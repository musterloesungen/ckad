## Übungsaufgabe für CKAD Prüfung: Implementierung eines Persistent Volume Claims

**Ziel:** Erstellung und Anbindung eines Persistent Volume Claims (PVC) an einen Pod in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Erstellen eines Persistent Volume Claims**
   - Erstelle einen PVC mit dem Namen `mein-pvc`.
   - Der PVC sollte 1 GB Speicher anfordern und das Access Mode `ReadWriteOnce` verwenden.

2. **Erstellen eines Pods, der den PVC nutzt**
   - Erstelle einen Pod mit dem Namen `mein-pod`.
   - Der Pod soll einen Container mit einem beliebigen Image (z.B. `nginx`) enthalten.
   - Der PVC `mein-pvc` soll als Volume in den Pod eingebunden werden.
   - Das Volume soll in einem Verzeichnis innerhalb des Containers gemountet werden (z.B. `/meine-daten`).

3. **Testen des PVCs im Pod**
   - Schreibe Daten in das gemountete Volume innerhalb des Pods.
   - Lösche den Pod und erstelle ihn neu.
   - Überprüfe, ob die zuvor geschriebenen Daten im Volume erhalten bleiben.

**Hinweise:**
- Beachte die korrekte Konfiguration des PVCs, um sicherzustellen, dass er den Speicheranforderungen entspricht.
- Achte darauf, dass das Volume korrekt in den Pod eingebunden und gemountet wird.

**Erfolgskriterien:**
- Ein PVC namens `mein-pvc` mit der angeforderten Größe und dem Access Mode ist erfolgreich erstellt.
- Ein Pod namens `mein-pod`, der den PVC nutzt, ist erfolgreich erstellt und ausgeführt.
- Das Volume wird korrekt in den Pod eingebunden und die Persistenz der Daten wird durch den Test bestätigt.
