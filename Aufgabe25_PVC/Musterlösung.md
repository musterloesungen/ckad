## Musterlösung: Implementierung eines Persistent Volume Claims

**Schritt 1: Erstellen eines Persistent Volume Claims**

Erstelle einen Persistent Volume Claim (PVC) `mein-pvc.yaml`:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mein-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

Wende den PVC an:

```bash
kubectl apply -f mein-pvc.yaml
```

**Schritt 2: Erstellen eines Pods, der den PVC nutzt**

Erstelle einen Pod `mein-pod.yaml`, der den PVC verwendet:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mein-pod
spec:
  containers:
    - name: nginx
      image: nginx
      volumeMounts:
      - mountPath: "/meine-daten"
        name: mein-volume
  volumes:
    - name: mein-volume
      persistentVolumeClaim:
        claimName: mein-pvc
```

Wende den Pod an:

```bash
kubectl apply -f mein-pod.yaml
```

**Schritt 3: Testen des PVCs im Pod**

1. **Schreiben von Daten in das Volume:**

   Führe in den Pod ein und erstelle eine Datei im gemounteten Volume:

   ```bash
   kubectl exec mein-pod -- sh -c 'echo "Testdaten" > /meine-daten/testdatei.txt'
   ```

2. **Überprüfen der Datenpersistenz:**

   Lösche den Pod und erstelle ihn neu:

   ```bash
   kubectl delete pod mein-pod
   kubectl apply -f mein-pod.yaml
   ```

   Überprüfe dann, ob die Daten erhalten geblieben sind:

   ```bash
   kubectl exec mein-pod -- cat /meine-daten/testdatei.txt
   ```

**Erwartetes Ergebnis:**
- Der PVC `mein-pvc` ist erfolgreich mit den Spezifikationen erstellt.
- Der Pod `mein-pod` nutzt das Volume, das durch den PVC bereitgestellt wird, erfolgreich.
- Die Daten, die im Volume gespeichert wurden, bleiben erhalten, auch nachdem der Pod gelöscht und neu erstellt wurde.

