## Musterlösung: Erstellung und Management eines StatefulSets

**Schritt 1: Erstellen eines StatefulSets**

Erstelle ein StatefulSet `mein-statefulset.yaml`:

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mein-statefulset
spec:
  serviceName: "mein-service"
  replicas: 3
  selector:
    matchLabels:
      app: mein-sts
  template:
    metadata:
      labels:
        app: mein-sts
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        volumeMounts:
        - name: mein-volume
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: mein-volume
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```

Wende das StatefulSet an:

```bash
kubectl apply -f mein-statefulset.yaml
```

**Schritt 2: Erstellen eines Headless Service**

Erstelle einen Headless Service `mein-service.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: mein-service
spec:
  clusterIP: None
  selector:
    app: mein-sts
  ports:
  - protocol: TCP
    port: 80
```

Wende den Service an:

```bash
kubectl apply -f mein-service.yaml
```

**Schritt 3: Überprüfung und Skalierung**

Überprüfe den Status des StatefulSets:

```bash
kubectl get statefulset mein-statefulset
```

Skaliere das StatefulSet auf 5 Replikate:

```bash
kubectl scale statefulsets mein-statefulset --replicas=5
```

Überprüfe erneut den Status:

```bash
kubectl get statefulset mein-statefulset
```

**Schritt 4: Testen der Datenpersistenz**

Teste die Datenpersistenz, indem du Daten in einen Pod schreibst:

```bash
kubectl exec mein-statefulset-0 -- bash -c 'echo "Testdaten" > /usr/share/nginx/html/index.html'
```

Lösche dann den Pod:

```bash
kubectl delete pod mein-statefulset-0
```

Nachdem der Pod neu erstellt wurde, überprüfe, ob die Daten erhalten geblieben sind:

```bash
kubectl exec mein-statefulset-0 -- cat /usr/share/nginx/html/index.html
```

**Erwartetes Ergebnis:**
- Das StatefulSet `mein-statefulset` ist mit 3 Replikaten erstellt und erfolgreich auf 5 skaliert.
- Der Headless Service `mein-service` ist korrekt konfiguriert und funktioniert.
- Die Datenpersistenz wird bestätigt, nachdem der Pod `mein-statefulset-0` gelöscht und neu erstellt wurde.
