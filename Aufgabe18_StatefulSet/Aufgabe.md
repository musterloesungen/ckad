## Übungsaufgabe für CKAD Prüfung: Erstellung und Management eines StatefulSets

**Ziel:** Erfolgreiches Erstellen und Verwalten eines StatefulSets in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Erstellen eines StatefulSets**
    - Erstelle ein StatefulSet mit dem Namen `mein-statefulset`.
    - Verwende als Image `nginx:latest`.
    - Das StatefulSet soll 3 Replikate haben.
    - Jeder Pod soll einen PersistentVolumeClaim für eine PersistentVolume nutzen, der 1 GB Speicherplatz bereitstellt.

2. **Erstellen eines Headless Service**
    - Erstelle einen Headless Service, um das Netzwerk-Identity-Management für die Pods im StatefulSet zu ermöglichen.

3. **Überprüfung und Skalierung**
    - Überprüfe den Status des StatefulSets und stelle sicher, dass alle Pods laufen.
    - Skaliere das StatefulSet auf 5 Replikate und überprüfe den Status erneut.

4. **Datenpersistenz testen**
    - Schreibe Daten in einen Pod und lösche dann diesen Pod.
    - Stelle sicher, dass die Daten erhalten bleiben, wenn der Pod neu erstellt wird.

**Hinweise:**
- Beachte die Besonderheiten von StatefulSets, insbesondere im Hinblick auf die stabile Netzwerkidentität und die Speicherpersistenz.
- Stelle sicher, dass die PersistentVolumeClaims und der Headless Service korrekt konfiguriert sind.

**Erfolgskriterien:**
- Ein StatefulSet `mein-statefulset` mit 3 Replikaten ist erfolgreich erstellt.
- Ein Headless Service für das StatefulSet ist konfiguriert.
- Das StatefulSet ist erfolgreich auf 5 Replikate skaliert.
- Die Datenpersistenz wird nach dem Löschen und Neuerstellen eines Pods verifiziert.
