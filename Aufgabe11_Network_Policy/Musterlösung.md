## Musterlösung: Netzwerkrichtlinien

### Namespace Manifest

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-net-ns
```

### Pod Manifest für Web-App

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ckad-web-pod
  namespace: ckad-net-ns
  labels:
    app: ckad-web
spec:
  containers:
  - name: web
    image: nginx
    ports:
    - containerPort: 8080
```

### Netzwerkrichtlinien Manifest

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: ckad-netpolicy
  namespace: ckad-net-ns
spec:
  podSelector:
    matchLabels:
      app: ckad-web
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          access: granted
    ports:
    - protocol: TCP
      port: 8080
```

### Anweisungen zur Anwendung und Überprüfung

1. Erstelle den Namespace `ckad-net-ns`:

```bash
kubectl apply -f namespace.yaml
```

2. Erstelle den Pod `ckad-web-pod` mit dem Webserver:

```bash
kubectl apply -f web-pod.yaml
```

3. Wende die Netzwerkrichtlinie `ckad-netpolicy` an:

```bash
kubectl apply -f netpolicy.yaml
```

4. Überprüfe die Netzwerkrichtlinie:

```bash
kubectl describe netpol ckad-netpolicy -n ckad-net-ns
```

Dieser Befehl zeigt die Details der Netzwerkrichtlinie, einschließlich der Selektoren und der konfigurierten Ports. Du kannst zusätzliche Tests durchführen, indem du versuchst, mit anderen Pods innerhalb des Clusters eine Verbindung zum Pod `ckad-web-pod` herzustellen, um die Wirksamkeit der Netzwerkrichtlinie zu bestätigen. Pods, die nicht das Label `access: granted` haben, sollten keine Verbindung zum Pod `ckad-web-pod` auf Port `8080` herstellen können.
