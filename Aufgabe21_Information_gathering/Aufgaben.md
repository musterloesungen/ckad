## CKAD Übungsaufgaben: Informationsbeschaffung

### Aufgabe 1: Auflistung von Pods im Standard-Namespace
- **Ziel:** Erstelle eine Liste aller aktiven Pods im `default` Namespace.
- **Aufgabe:**
    - Finde heraus, wie du alle laufenden Pods im `default` Namespace abfragen kannst, und schreibe diese Informationen in die Datei `/mnt/data/pod_names.txt`.

### Aufgabe 2: Erfassung von Cluster-Knoteninformationen
- **Ziel:** Sammle umfassende Informationen über alle Knoten im Cluster.
- **Aufgabe:**
    - Ermittle, wie du detaillierte Informationen zu allen Knoten im Cluster abrufen kannst, und speichere diese Daten in `/mnt/data/nodes_info.txt`.

### Aufgabe 3: Erfassen der jüngsten Cluster-Events
- **Ziel:** Erhalte einen Überblick über die jüngsten Ereignisse im Cluster.
- **Aufgabe:**
    - Identifiziere eine Methode, um die neuesten Cluster-Events abzurufen, und leite die letzten 50 Events in die Datei `/mnt/data/recent_events.txt`.

### Aufgabe 4: Protokollierung von Container-Aktivitäten
- **Ziel:** Sammle Log-Daten eines spezifischen, aktiven Containers.
- **Aufgabe:**
    - Finde den passenden Befehl, um die Logs des neuesten Pods mit dem Label `app=myapp` zu erhalten, und speichere diese Logs in `/mnt/data/myapp_logs.txt`.

### Aufgabe 5: Analyse einer Deployment-Konfiguration
- **Ziel:** Untersuche die Konfiguration eines spezifischen Deployments.
- **Aufgabe:**
    - Ermittle, wie die Konfiguration eines Deployments namens `my-deployment` im Namespace `production` abgefragt und in YAML-Format in die Datei `/mnt/data/deployment_configuration.yaml` geschrieben werden kann.

