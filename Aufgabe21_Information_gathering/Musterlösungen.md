### Lösung für Aufgabe 1: Auflistung von Pods im Standard-Namespace
- **Befehl:**
  ```bash
  kubectl get pods -n default > /mnt/data/pod_names.txt
  ```

### Lösung für Aufgabe 2: Erfassung von Cluster-Knoteninformationen
- **Befehl:**
  ```bash
  kubectl get nodes -o wide > /mnt/data/nodes_info.txt
  ```

### Lösung für Aufgabe 3: Erfassen der jüngsten Cluster-Events
- **Befehl:**
  ```bash
  kubectl get events --sort-by='.lastTimestamp' | tail -n 50 > /mnt/data/recent_events.txt
  ```

### Lösung für Aufgabe 4: Protokollierung von Container-Aktivitäten
- **Befehle:**
    1. Identifizieren des neuesten Pod-Namens:
       ```bash
       POD_NAME=$(kubectl get pods -l app=myapp --sort-by=.metadata.creationTimestamp -o jsonpath="{.items[-1].metadata.name}")
       ```
    2. Extrahieren und Speichern der Logs:
       ```bash
       kubectl logs $POD_NAME > /mnt/data/myapp_logs.txt
       ```

### Lösung für Aufgabe 5: Analyse einer Deployment-Konfiguration
- **Befehl:**
  ```bash
  kubectl get deployment my-deployment -n production -o yaml > /mnt/data/deployment_configuration.yaml
  ```
