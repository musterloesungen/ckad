## Musterlösung: Implementierung eines Canary Deployments

**Schritt 1: Erstellen eines Baseline-Deployments**

Erstelle ein Baseline Deployment `baseline-app-deployment.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: baseline-app-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-app
      version: v1.16
  template:
    metadata:
      labels:
        app: nginx-app
        version: v1.16
    spec:
      containers:
      - name: nginx
        image: nginx:1.16
```

Wende das Baseline Deployment an:

```bash
kubectl apply -f baseline-app-deployment.yaml
```

**Schritt 2: Implementierung des Canary Deployments**

Erstelle ein Canary Deployment `canary-app-deployment.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: canary-app-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-app
      version: v1.17
  template:
    metadata:
      labels:
        app: nginx-app
        version: v1.17
    spec:
      containers:
      - name: nginx
        image: nginx:1.17
```

Wende das Canary Deployment an:

```bash
kubectl apply -f canary-app-deployment.yaml
```

**Schritt 3: Routing des Traffics**

Erstelle einen Service `app-service.yaml`, der beide Deployments abdeckt, aber einen Teil des Traffics gezielt zum Canary leitet:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: nginx-app
```

**Schritt 4: Überwachung und Skalierung**

Überwache das Verhalten des Canary Deployments, z.B. durch Überprüfung der Logs oder Metriken. Bei positivem Ergebnis skaliere das Canary Deployment hoch und das Baseline Deployment herunter:

```bash
kubectl scale deployment canary-app-deployment --replicas=3
kubectl scale deployment baseline-app-deployment --replicas=0
```

**Erwartetes Ergebnis:**
- Das Baseline Deployment `baseline-app-deployment` ist mit 3 Replikaten des `nginx:1.16` Images erstellt.
- Das Canary Deployment `canary-app-deployment` ist mit 1 Replikat des `nginx:1.17` Images erstellt.
- Der Service `nginx-service` leitet Traffic sowohl an das Baseline als auch an das Canary Deployment.
- Nach erfolgreicher Überwachung wird das Canary Deployment zum neuen Baseline, indem es vollständig hochskaliert und das alte Baseline Deployment heruntergefahren wird.
