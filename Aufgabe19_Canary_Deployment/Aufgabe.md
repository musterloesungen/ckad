## Übungsaufgabe für CKAD Prüfung: Implementierung eines Canary Deployments

**Ziel:** Implementieren und Verwalten eines Canary Deployments in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Erstellen eines Baseline-Deployments**
    - Erstelle ein Deployment mit dem Namen `baseline-app-deployment`.
    - Verwende das Image `nginx:1.16` und stelle sicher, dass 3 Replikate des Pods laufen.
    - Erstelle einen Service, der dieses Deployment abbildet.

2. **Implementierung des Canary Deployments**
    - Erstelle ein neues Deployment mit dem Namen `canary-app-deployment`, das eine neue Version des Images (z.B. `nginx:1.17`) verwendet.
    - Dieses Deployment sollte anfangs nur mit einem Replikat laufen.

3. **Routing des Traffics**
    - Konfiguriere den bestehenden Service so, dass ein kleiner Prozentsatz des Traffics (z.B. 10%) an das Canary Deployment und der Rest an das Baseline Deployment geroutet wird.

4. **Überwachung und Skalierung**
    - Überwache das Verhalten des Canary Deployments.
    - Bei Erfolg, skaliere schrittweise das Canary Deployment hoch und das Baseline Deployment herunter, bis das Canary Deployment das Baseline Deployment vollständig ersetzt hat.

**Hinweise:**
- Achte darauf, die richtigen Selektoren und Labels zu verwenden, um den Traffic korrekt zwischen den Deployments zu steuern.
- Canary Deployments sind nützlich, um neue Versionen einer Anwendung schrittweise mit minimalem Risiko einzuführen.

**Erfolgskriterien:**
- Ein Baseline Deployment `baseline-app-deployment` und ein Canary Deployment `canary-app-deployment` sind erfolgreich erstellt.
- Der Service leitet einen definierten Prozentsatz des Traffics an das Canary Deployment weiter.
- Das Canary Deployment wird erfolgreich überwacht und schrittweise skaliert, bis es das Baseline Deployment vollständig ersetzt.
