## CKAD Trainingsaufgabe: Erstellung eines Multi-Container Pods

1. Definiere ein Pod-Manifest für einen Pod namens `ckad-multi-container-pod` im Namespace `ckad-multi-ns`.
2. Der Pod soll zwei Container enthalten:
   - Der erste Container, benannt `primary-container`, soll das Image `nginx` verwenden und auf Port `80` laufen.
   - Der zweite Container, benannt `helper-container`, soll das Image `busybox` verwenden und eine Endlosschleife ausführen, die alle 5 Sekunden `date` ausgibt (`while true; do date; sleep 5; done`).
3. Konfiguriere einen gemeinsamen EmptyDir Volume namens `shared-logs` für beide Container, in dem der `helper-container` Ausgaben an eine Datei `current-date.txt` im Verzeichnis `/var/log` schreibt.
4. Der `primary-container` soll ebenfalls Zugriff auf das gemeinsame Volume haben, um die Datei `current-date.txt` lesen zu können.
5. Erstelle den Namespace `ckad-multi-ns`, falls er nicht existiert.
6. Wende das Pod-Manifest an und überprüfe, ob beide Container erfolgreich gestartet sind und das Volume wie erwartet genutzt wird.