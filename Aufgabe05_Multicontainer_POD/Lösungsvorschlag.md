## Musterlösung: YAML-Definition für Multi-Container Pod

### Pod Manifest

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: ckad-multi-container-pod
  namespace: ckad-multi-ns
spec:
  volumes:
  - name: shared-logs
    emptyDir: {}
  containers:
  - name: primary-container
    image: nginx
    ports:
    - containerPort: 80
    volumeMounts:
    - name: shared-logs
      mountPath: /usr/share/nginx/html
  - name: helper-container
    image: busybox
    command: ["/bin/sh"]
    args: ["-c", "while true; do date >> /var/log/current-date.txt; sleep 5; done"]
    volumeMounts:
    - name: shared-logs
      mountPath: /var/log
```

### Namespace Manifest

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-multi-ns
```

### Anweisungen zur Anwendung und Überprüfung des Pods

1. Erstelle den Namespace `ckad-multi-ns` mit dem folgenden Befehl:

   ```bash
   kubectl apply -f namespace-manifest.yaml
   ```

2. Wende das Pod-Manifest an, um den Multi-Container Pod zu erstellen:

   ```bash
   kubectl apply -f pod-manifest.yaml
   ```

3. Überprüfe den Status des Pods, um sicherzustellen, dass beide Container erfolgreich gestartet wurden:

   ```bash
   kubectl get pods -n ckad-multi-ns
   ```

4. Um zu überprüfen, ob der `helper-container` die `current-date.txt` Datei schreibt, führe folgenden Befehl aus:

   ```bash
   kubectl exec ckad-multi-container-pod -n ckad-multi-ns -c helper-container -- cat /var/log/current-date.txt
   ```

5. Um sicherzustellen, dass der `primary-container` Zugriff auf die `current-date.txt` Datei hat, führe folgenden Befehl aus:

   ```bash
   kubectl exec ckad-multi-container-pod -n ckad-multi-ns -c primary-container -- cat /usr/share/nginx/html/current-date.txt
   ```