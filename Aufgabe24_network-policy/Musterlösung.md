## Musterlösung: Erstellung und Management einer Network Policy

**Schritt 1: Erstellung zweier Deployments**

Erstelle zwei Deployments: `frontend-deployment.yaml` und `backend-deployment.yaml`.

**Frontend Deployment:**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: frontend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: frontend
  template:
    metadata:
      labels:
        app: frontend
    spec:
      containers:
      - name: nginx
        image: nginx
```

**Backend Deployment:**

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - name: nginx
        image: nginx
```

Wende die Deployments an:

```bash
kubectl apply -f frontend-deployment.yaml
kubectl apply -f backend-deployment.yaml
```

**Schritt 2: Erstellung von Services**

Erstelle Services für jedes Deployment: `frontend-service.yaml` und `backend-service.yaml`.

**Frontend Service:**

```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend-service
spec:
  selector:
    app: frontend
  ports:
    - protocol: TCP
      port: 80
```

**Backend Service:**

```yaml
apiVersion: v1
kind: Service
metadata:
  name: backend-service
spec:
  selector:
    app: backend
  ports:
    - protocol: TCP
      port: 80
```

Wende die Services an:

```bash
kubectl apply -f frontend-service.yaml
kubectl apply -f backend-service.yaml
```

**Schritt 3: Implementierung einer Network Policy**

Erstelle eine Network Policy `network-policy.yaml`, die den Zugriff des Frontends auf das Backend erlaubt:

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: frontend-to-backend
spec:
  podSelector:
    matchLabels:
      app: backend
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          app: frontend
    ports:
    - protocol: TCP
      port: 80
```

Wende die Network Policy an:

```bash
kubectl apply -f network-policy.yaml
```

**Schritt 4: Testen der Konfiguration**

Um die Konfiguration zu testen:

1. Erstelle einen temporären Pod im Frontend und versuche, das Backend zu erreichen.
2. Versuche das Gleiche vom Backend zum Frontend, was fehlschlagen sollte.

**Überprüfung:**

```bash
kubectl run temp-frontend --image=busybox --rm -it --restart=Never -- wget -O- http://backend-service
kubectl run temp-backend --image=busybox --rm -it --restart=Never -- wget -O- http://frontend-service
```

**Erwartetes Ergebnis:**
- Der Zugriff vom Frontend auf das Backend funktioniert, während der umgekehrte Zugriff blockiert wird.
- Die Deployments `frontend` und `backend` sowie die dazugehörigen Services sind korrekt erstellt.
- Die Network Policy regelt den Netzwerkverkehr entsprechend den Anforderungen.

