## Übungsaufgabe für CKAD Prüfung: Erstellung und Management eines Network Policies

**Ziel:** Entwerfen und Implementieren einer Network Policy in einem Kubernetes Cluster.

**Aufgabenstellung:**

1. **Erstellung zweier Deployments**
   - Erstelle zwei Deployments: `frontend` und `backend`.
   - Jedes Deployment soll einen einfachen nginx-Container verwenden.
   - Weise jedem Deployment spezifische Labels zu (`app: frontend` für das Frontend und `app: backend` für das Backend).

2. **Erstellung von Services**
   - Erstelle für jedes Deployment einen entsprechenden Service.
   - Der Service für das Frontend sollte das Frontend-Deployment abbilden und der Service für das Backend das Backend-Deployment.

3. **Implementierung einer Network Policy**
   - Erstelle eine Network Policy, die den Zugriff des Frontend-Deployments auf das Backend-Deployment erlaubt.
   - Das Frontend-Deployment sollte in der Lage sein, Anfragen an das Backend-Deployment zu senden, aber nicht umgekehrt.
   - Stelle sicher, dass die Network Policy spezifisch für den Verkehr zwischen diesen beiden Deployments gilt.

4. **Testen der Konfiguration**
   - Teste die Konfiguration, indem du versuchst, vom Frontend-Pod aus eine Verbindung zum Backend-Pod herzustellen.
   - Überprüfe auch, dass das Backend keine Verbindung zum Frontend herstellen kann.

**Hinweise:**
- Beachte beim Erstellen der Network Policy die korrekten Labels und Selektoren.
- Stelle sicher, dass die Network Policy so konfiguriert ist, dass sie den Anforderungen der Aufgabe entspricht.

**Erfolgskriterien:**
- Zwei Deployments `frontend` und `backend` sowie die dazugehörigen Services sind erfolgreich erstellt.
- Eine Network Policy ist implementiert, die den spezifizierten Netzwerkverkehr zwischen den Deployments regelt.
- Die Funktionsweise der Network Policy ist durch erfolgreiche Tests verifiziert.
