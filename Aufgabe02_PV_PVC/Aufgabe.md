## CKAD Trainingsaufgabe: Konfiguration eines persistenten Speichers

1. Erstelle ein Persistent Volume (PV) mit dem Namen `ckad-pv`, das 1 GiB Speicherplatz bereitstellt und das `ReadWriteOnce` Zugriffsmodell unterstützt.
2. Definiere ein Persistent Volume Claim (PVC) namens `ckad-pvc`, das das eben erstellte PV beansprucht und genau 1 GiB Speicher anfordert.
3. Erstelle ein Deployment namens `ckad-storage-app`, das das Image `busybox` verwendet.
4. Das Deployment soll einen einzigen Pod mit einem Container starten, der eine Endlosschleife ausführt (`while true; do sleep 3600; done`).
5. Binde das erstellte PVC als Volume in den Container ein, und zwar unter dem Mount-Pfad `/ckad-data`.
6. Stelle sicher, dass das Deployment im Namespace `ckad-storage` erfolgt.
7. Erstelle den Namespace, das PV, das PVC und das Deployment mithilfe entsprechender YAML-Dateien.
8. Überprüfe nach dem Erstellen, ob das PVC erfolgreich an den Pod gebunden wurde und der Pod im `Running`-Status ist.