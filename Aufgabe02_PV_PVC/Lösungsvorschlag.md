## Musterlösung: YAML-Definitionen für Persistent Storage

### Persistent Volume

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: ckad-pv
  labels:
    type: local
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```

### Persistent Volume Claim

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ckad-pvc
  namespace: ckad-storage
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

### Namespace

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-storage
```

### Deployment

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-storage-app
  namespace: ckad-storage
spec:
  selector:
    matchLabels:
      app: ckad-storage-app
  template:
    metadata:
      labels:
        app: ckad-storage-app
    spec:
      containers:
      - name: busybox
        image: busybox
        command: ["sh", "-c", "while true; do sleep 3600; done"]
        volumeMounts:
        - name: ckad-storage-volume
          mountPath: "/ckad-data"
      volumes:
      - name: ckad-storage-volume
        persistentVolumeClaim:
          claimName: ckad-pvc
```

### Anweisungen zur Überprüfung

1. Wende alle YAML-Definitionen mit kubectl an:

```bash
kubectl apply -f namespace.yaml
kubectl apply -f persistent-volume.yaml
kubectl apply -f persistent-volume-claim.yaml
kubectl apply -f deployment.yaml
```

2. Überprüfe den Status des Persistent Volume Claims:

```bash
kubectl get pvc -n ckad-storage
```

3. Überprüfe den Status des Deployments und der Pods:

```bash
kubectl get deployments -n ckad-storage
kubectl get pods -n ckad-storage
```

4. Bestätige, dass das Volume korrekt an den Pod gebunden wurde:

```bash
kubectl describe pod -l app=ckad-storage-app -n ckad-storage
```