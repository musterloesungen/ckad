## Musterlösung: Ressourcenbeschränkungen in einem Deployment

### Namespace Manifest

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-resource-ns
```

### Deployment Manifest

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-resource-deployment
  namespace: ckad-resource-ns
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:stable
        ports:
        - containerPort: 80
        resources:
          requests:
            memory: "64Mi"
            cpu: "250m"
          limits:
            memory: "128Mi"
            cpu: "500m"
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 10
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 5
          periodSeconds: 5
```

### Anweisungen zur Anwendung und Überprüfung

1. Erstelle den Namespace `ckad-resource-ns`:

```bash
kubectl apply -f namespace.yaml
```

2. Wende das Deployment-Manifest an:

```bash
kubectl apply -f deployment.yaml
```

3. Überprüfe den Status des Deployments:

```bash
kubectl get deployment ckad-resource-deployment -n ckad-resource-ns
```

4. Überprüfe die Ressourcenbeschränkungen und Zustände der Pods:

```bash
kubectl get pods -n ckad-resource-ns -o wide
kubectl describe pod -l app=nginx -n ckad-resource-ns
```

In der Beschreibung der Pods unter "Containers" solltest du die konfigurierten Ressourcenanforderungen und -grenzen sehen können. Weiterhin werden Liveness und Readiness Probes angezeigt, die die Verfügbarkeit der Pods sicherstellen.
