## CKAD Trainingsaufgabe: Ressourcenbeschränkungen in einem Deployment

1. Erstelle ein Deployment namens `ckad-resource-deployment` im Namespace `ckad-resource-ns`.
2. Das Deployment soll das Image `nginx:stable` verwenden und drei Replikate des Pods starten.
3. Definiere Ressourcenlimits von `500m` CPU und `128Mi` Speicher für jeden Pod im Deployment.
4. Setze Ressourcenanforderungen (requests) von `250m` CPU und `64Mi` Speicher für jeden Pod.
5. Der Pod soll einen Liveness Check über HTTP auf dem Pfad `/` und Port `80` durchführen.
6. Der Pod soll auch einen Readiness Check über HTTP auf dem Pfad `/` und Port `80` durchführen.
7. Stelle sicher, dass der Namespace `ckad-resource-ns` vor der Erstellung des Deployments existiert.
8. Nach der Erstellung des Deployments, überprüfe die Ressourcenzuweisungen und die Zustände der Pods, um sicherzustellen, dass die Ressourcenbeschränkungen korrekt angewendet wurden.
