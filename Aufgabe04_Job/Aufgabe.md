## CKAD Trainingsaufgabe: Erstellen eines Kubernetes Jobs

1. Erstelle ein Job-Manifest für einen Job namens `ckad-example-job`, der das Image `busybox` verwendet.
2. Der Job soll den Befehl `echo "Hello from the CKAD Job"` ausführen.
3. Konfiguriere den Job so, dass er nur einmal ausgeführt wird und nach erfolgreicher Ausführung des Befehls beendet wird.
4. Stelle sicher, dass der Job im Namespace `ckad-job-ns` läuft.
5. Erstelle den Namespace, falls er nicht existiert.
6. Wende das Manifest an und beobachte den Status des Jobs, um sicherzustellen, dass er erfolgreich abgeschlossen wurde.
7. Extrahiere und überprüfe die Logs des Job-Pods, um die erfolgreiche Ausführung des Befehls zu bestätigen.