## Musterlösung: YAML-Definition für Kubernetes Job

### Job Manifest

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: ckad-example-job
  namespace: ckad-job-ns
spec:
  template:
    spec:
      containers:
      - name: hello
        image: busybox
        command: ["sh", "-c", "echo 'Hello from the CKAD Job'"]
      restartPolicy: Never
  backoffLimit: 0
```

### Namespace Manifest

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ckad-job-ns
```

### Anweisungen zur Anwendung und Überprüfung des Jobs

1. Erstelle den Namespace `ckad-job-ns` mit dem folgenden Befehl:

   ```bash
   kubectl apply -f namespace-manifest.yaml
   ```

2. Wende das Job-Manifest an, um den Job zu erstellen:

   ```bash
   kubectl apply -f job-manifest.yaml
   ```

3. Überwache den Job-Status, um zu bestätigen, dass er ausgeführt wurde und erfolgreich beendet ist:

   ```bash
   kubectl get jobs -n ckad-job-ns
   ```

4. Sobald der Job beendet ist, überprüfe die Logs des Pods, der vom Job erstellt wurde:

   ```bash
   kubectl logs job/ckad-example-job -n ckad-job-ns
   ```

   Der Befehl `kubectl logs` erwartet den Pod-Namen. Da Jobs zufällige Pod-Namen generieren, kannst du den genauen Pod-Namen mit folgendem Befehl finden und dann die Logs extrahieren:

   ```bash
   POD_NAME=$(kubectl get pods -n ckad-job-ns --selector=job-name=ckad-example-job --output=jsonpath='{.items[*].metadata.name}')
   kubectl logs $POD_NAME -n ckad-job-ns
   ```