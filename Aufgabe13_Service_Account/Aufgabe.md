## CKAD Übungsaufgabe: Authentifizierung und Service Accounts in Kubernetes

### Szenario
Als Teil Ihrer Arbeit als Kubernetes-Entwickler müssen Sie sicherstellen, dass Anwendungen und Prozesse im Cluster korrekt authentifiziert werden. Sie werden einen Service Account erstellen und ihn für die Authentifizierung verwenden, um mit dem Kubernetes-Cluster zu interagieren.

### Aufgaben

#### Aufgabe 1: Erstellen eines Service Accounts

1. Erstellen Sie einen neuen Service Account namens `dev-user` im Namespace `development`. Wenn der Namespace nicht existiert, erstellen Sie ihn.
2. Listet das automatisch generierte Secret für den `dev-user` Service Account auf.

#### Aufgabe 2: Token des Service Accounts extrahieren

1. Extrahieren Sie das Token aus dem Secret, das für den `dev-user` Service Account erstellt wurde.
2. Verwenden Sie das extrahierte Token, um mit `kubectl` eine Abfrage für die im Namespace `development` laufenden Pods zu authentifizieren.

#### Aufgabe 3: Einrichten von Role-Based Access Control (RBAC)

1. Erstellen Sie eine Rolle namens `dev-reader`, die Lesezugriff (`get`, `list`, `watch`) auf Ressourcen vom Typ `pods` im Namespace `development` erlaubt.
2. Binden Sie die Rolle `dev-reader` an den Service Account `dev-user` durch eine RoleBinding.

#### Aufgabe 4: Verwenden des Service Accounts für eine API-Anfrage

1. Verwenden Sie das Token des Service Accounts, um eine Abfrage mit `curl` gegen die Kubernetes-API auszuführen, um die Pods im Namespace `development` aufzulisten.

### Anleitung

Hier sind einige `kubectl`-Befehle, die Ihnen bei der Lösung der Aufgaben helfen:

- Erstellen eines Namespaces:
  ```sh
  kubectl create namespace development
  ```

- Erstellen eines Service Accounts:
  ```sh
  kubectl create serviceaccount dev-user --namespace development
  ```

- Anzeigen des Secrets für einen Service Account:
  ```sh
  kubectl get serviceaccounts dev-user --namespace development -o jsonpath="{.secrets[*]['name']}"
  ```

- Extrahieren des Tokens aus einem Secret:
  ```sh
  kubectl get secret <secret-name> --namespace development -o jsonpath="{.data.token}" | base64 --decode
  ```

- Erstellen einer Rolle:
  ```yaml
  apiVersion: rbac.authorization.k8s.io/v1
  kind: Role
  metadata:
    namespace: development
    name: dev-reader
  rules:
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["get", "watch", "list"]
  ```

- Erstellen einer RoleBinding:
  ```yaml
  apiVersion: rbac.authorization.k8s.io/v1
  kind: RoleBinding
  metadata:
    name: dev-reader-binding
    namespace: development
  subjects:
  - kind: ServiceAccount
    name: dev-user
    namespace: development
  roleRef:
    kind: Role
    name: dev-reader
    apiGroup: rbac.authorization.k8s.io
  ```
