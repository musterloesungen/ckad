## Musterlösungen: Authentifizierung und Service Accounts in Kubernetes

### Lösung Aufgabe 1: Erstellen eines Service Accounts

```sh
# Namespace erstellen, falls er nicht existiert
kubectl create namespace development

# Service Account im Namespace development erstellen
kubectl create serviceaccount dev-user --namespace development

# Secret für den Service Account auflisten
kubectl get serviceaccounts dev-user --namespace development -o jsonpath="{.secrets[*]['name']}"
```

### Lösung Aufgabe 2: Token des Service Accounts extrahieren

```sh
# Secret-Name extrahieren (ersetzen Sie <secret-name> mit dem tatsächlichen Namen)
SECRET_NAME=$(kubectl get serviceaccount dev-user --namespace development -o jsonpath="{.secrets[0].name}")

# Token aus dem Secret extrahieren
kubectl get secret $SECRET_NAME --namespace development -o jsonpath="{.data.token}" | base64 --decode
```

### Lösung Aufgabe 3: Einrichten von Role-Based Access Control (RBAC)

1. Rolle `dev-reader` erstellen:

    ```yaml
    apiVersion: rbac.authorization.k8s.io/v1
    kind: Role
    metadata:
      namespace: development
      name: dev-reader
    rules:
    - apiGroups: [""]
      resources: ["pods"]
      verbs: ["get", "watch", "list"]
    ```

   Speichern Sie den obigen Inhalt in einer Datei namens `dev-reader-role.yaml` und führen Sie den folgenden Befehl aus:

    ```sh
    kubectl apply -f dev-reader-role.yaml
    ```

2. RoleBinding erstellen, um die Rolle `dev-reader` an den Service Account `dev-user` zu binden:

    ```yaml
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
      name: dev-reader-binding
      namespace: development
    subjects:
    - kind: ServiceAccount
      name: dev-user
      namespace: development
    roleRef:
      kind: Role
      name: dev-reader
      apiGroup: rbac.authorization.k8s.io
    ```

   Speichern Sie den obigen Inhalt in einer Datei namens `dev-reader-rolebinding.yaml` und führen Sie den folgenden Befehl aus:

    ```sh
    kubectl apply -f dev-reader-rolebinding.yaml
    ```

### Lösung Aufgabe 4: Verwenden des Service Accounts für eine API-Anfrage

```sh
# Setzen Sie TOKEN mit dem Wert, den Sie in Aufgabe 2 extrahiert haben
TOKEN="..."

# API-Anfrage mit curl ausführen
curl -k -H "Authorization: Bearer $TOKEN" https://<api-server-adresse>/api/v1/namespaces/development/pods
```

Ersetzen Sie `<api-server-adresse>` mit der tatsächlichen Adresse Ihres Kubernetes-API-Servers und `TOKEN` mit dem tatsächlichen Token, das Sie extrahiert haben. Beachten Sie, dass `-k` in `curl` verwendet wird, um die Überprüfung des SSL-Zertifikats zu umgehen, was in einer echten Umgebung nicht empfohlen wird.
