## Korrigiertes YAML-Definition: Deployment ohne Fehler

### Korrigiertes Manifest

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-error-app
  namespace: ckad-error-ns
spec:
  replicas: 2
  selector:
    matchLabels:
      app: ckad-error-app
  template:
    metadata:
      labels:
        app: ckad-error-app
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 80
```

### Anweisungen zur Fehlerbehebung und Überprüfung

1. Erstelle zuerst den Namespace (falls noch nicht vorhanden):

   ```bash
   kubectl create namespace ckad-error-ns
   ```

2. Wende das fehlerhafte Manifest an:

   ```bash
   kubectl apply -f fehlerhaftes-deployment.yaml
   ```

3. Überprüfe den Status des Deployments und der Pods:

   ```bash
   kubectl get deployments -n ckad-error-ns
   kubectl get pods -n ckad-error-ns
   ```

4. Beobachte die Events für Pods, um Fehlermeldungen zu identifizieren:

   ```bash
   kubectl get events -n ckad-error-ns
   ```

5. Nach Identifizierung des Fehlers (in diesem Fall die `imagePullPolicy`), korrigiere das Manifest, indem du die `imagePullPolicy` auf `IfNotPresent` änderst.

6. Wende das korrigierte Manifest an:

   ```bash
   kubectl apply -f korrigiertes-deployment.yaml
   ```

7. Überprüfe erneut den Status, um sicherzustellen, dass die Pods jetzt im `Running`-Status sind:

   ```bash
   kubectl get pods -n ckad-error-ns
   ```

Die Änderung der `imagePullPolicy` von `Never` zu `IfNotPresent` stellt sicher, dass das Image von einem Registry bezogen wird, falls es nicht lokal vorhanden ist, was in den meisten Fällen erforderlich ist, wenn man ein `nginx:latest` Image verwendet.