## CKAD Trainingsaufgabe: Fehlerbehebung in einem Deployment-Manifest

Hier ist ein Kubernetes Deployment-Manifest, das einen Konfigurationsfehler enthält. Deine Aufgabe ist es:

1. Wende das Manifest auf dein Kubernetes Cluster an.
2. Identifiziere den Fehler, der verhindert, dass die Pods im `Running`-Status sind.
3. Korrigiere den Fehler im Manifest.
4. Wende das korrigierte Manifest an, um ein funktionsfähiges Deployment sicherzustellen.
5. Überprüfe, ob die Pods jetzt erfolgreich gestartet und im `Running`-Status sind.

### Manifest mit Fehler

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-error-app
  namespace: ckad-error-ns
spec:
  replicas: 2
  selector:
    matchLabels:
      app: ckad-error-app
  template:
    metadata:
      labels:
        app: ckad-error-app
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        imagePullPolicy: Never
        ports:
        - containerPort: 80
```

Hinweis: Du musst eventuell zusätzliche Schritte durchführen, wie zum Beispiel den Namespace `ckad-error-ns` erstellen, falls er noch nicht existiert. Nutze die kubectl-Befehle, um das Manifest anzuwenden und den Status der Ressourcen zu überwachen.