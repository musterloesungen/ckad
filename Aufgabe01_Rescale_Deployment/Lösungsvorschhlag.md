## Musterlösung: YAML-Definition für CKAD Deployment

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ckad-app
  labels:
    app: ckad-app
    tier: frontend
  namespace: ckad-training
spec:
  replicas: 2
  selector:
    matchLabels:
      app: ckad-app
  template:
    metadata:
      labels:
        app: ckad-app
        tier: frontend
    spec:
      containers:
      - name: nginx
        image: nginx:1.25.3
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /
            port: 80
        readinessProbe:
          httpGet:
            path: /healthz
            port: 80
```
## Hinweis

Eine gute Vorlage zur Erstellung des yaml Files erhält man mit:

```bash
kubectl create deployment ckad-app --image=nginx:1.25.3 --dry-run=client -o yaml --replicas=2 --port=80 --namespace=ckad-training
```

Vorlagen für livenessProbe und readinessProbe kann man folgendem Link entnehmen:

[Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)


## Anweisungen zum Skalieren des Deployments

1. Wende die YAML-Definition auf dein Kubernetes Cluster an mit:

   ```bash
   kubectl apply -f deployment.yaml
   ```

2. Skaliere das Deployment auf vier Replikate mit:

   ```bash
   kubectl scale deployment ckad-app --replicas=4 -n ckad-training
   ```

3. Überwache die Pods mit:

   ```bash
   kubectl get pods -n ckad-training -w
   ```

4. Überprüfe die Logs eines Pods auf Liveness und Readiness Probes mit:

   ```bash
   kubectl logs <POD_NAME> -n ckad-training
   ```

Ersetze `<POD_NAME>` mit dem tatsächlichen Namen eines der Pods des Deployments.


