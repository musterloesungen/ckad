## CKAD Trainingsaufgabe: Erstellen und Skalieren eines Deployments

1. Erstelle ein YAML-Datei für ein Deployment namens `ckad-app`, das ein `nginx` Image verwendet.
2. Setze die Version des `nginx` Images auf `1.25.3`.
3. Definiere zwei Replikate des Pods im Deployment.
4. Stelle sicher, dass Liveness Probes über den HTTP-GET-Request auf Port `80` konfiguriert sind.
5. Konfiguriere Readiness Probes, die einen HTTP-GET-Request auf den Pfad `/healthz` und Port `80` ausführen.
6. Das Deployment sollte Labels `app: ckad-app` und `tier: frontend` verwenden.
7. Erstelle das Deployment im Namespace `ckad-training`.
8. Skaliere das Deployment auf vier Replikate, nachdem es erfolgreich erstellt wurde.
9. Überwache die Pods und stelle sicher, dass sie erfolgreich gestartet sind.
10. Teste die Liveness und Readiness Probes, indem du entsprechende Logs des nginx Containers ausgibst.
